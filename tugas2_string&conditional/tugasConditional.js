// soal nomor 1
var nama = "John"
var peran = "Werewolf";
var selamatDatang = `Selamat datang di dunia werewolf ${nama}`;
var pesan = '';

if(nama == "") {
    console.log('Nama Harus Diisi');
} 

else if(peran == 'Penyihir') {
    pesan = `Halo ${peran} ${nama}, kamu dapat melihat siapa yang menjadi werewolf`;
}

else if(peran == 'Guard') {
    pesan = `Halo ${peran} ${nama}, kamu akan membantu melindungi temanmu dari serangan werewolf`;
}

else if(peran == 'Werewolf'){
    pesan = `Halo ${peran} ${nama}, Kamu akan memakan mangsa setiap malam!`;
}

else {
    pesan = `Halo ${nama} silahkan pilih peranmu untuk memulai game`;
}

console.log(selamatDatang);
console.log(pesan);

//soal nomor 2
var tanggal = 4;
var bulan = 12;
var tahun = 2000;

switch(bulan) {
    case 1: {
        bulan = 'januari';
        break;
    }
    case 2: {
        bulan = 'februari';
        break;
    }
    case 3: {
        bulan = 'maret';
        break;
    }
    case 4: {
        bulan = 'april';
        break;
    }
    case 5: {
        bulan = 'mei';
        break;
    }
    case 6: {
        bulan = 'juni';
        break;
    }
    case 7: {
        bulan = 'juli';
        break;
    }
    case 8: {
        bulan = 'agustus';
        break;
    }
    case 9: {
        bulan = 'september';
        break;
    }
    case 10: {
        bulan = 'oktober';
        break;
    }
    case 11: {
        bulan = 'november';
        break;
    }
    case 12: {
        bulan = 'desember';
        break;
    }
    default: {
        console.log(`${tanggal} ${bulan} ${tahun}`)
    }
}

console.log(`${tanggal} ${bulan} ${tahun}`)