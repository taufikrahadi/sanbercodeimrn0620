// soal nomor 1 (membuat kalimat)
var word = 'JavaScript'; 
var second = 'is'; 
var third = 'awesome'; 
var fourth = 'and'; 
var fifth = 'I'; 
var sixth = 'love'; 
var seventh = 'it!';

console.log(word + ' ' + second + ' ' + third + ' ' + fourth + ' ' + fifth + ' ' + sixth + ' ' + seventh);

// soal nomor 2 (mengurai kalimat)
var sentence = 'I am going to be react native developer';
var firstWord = sentence[0];
var secondWord = sentence[2] + sentence[3];
var thirdWord = sentence[5] + sentence[6] + sentence[7] + sentence[8] + sentence[9];
var fourthWord = sentence[11] + sentence[12];
var fifthWord = sentence[14] + sentence[15];
var sixthWord = sentence[17] + sentence[18] + sentence[19] + sentence[20] + sentence[21];
var seventhWord = sentence[30] + sentence[31] + sentence[32] + sentence[33] + sentence[34] + sentence[35] + sentence[36] + sentence[37] + sentence[38];

console.log('First Word : ' + firstWord);
console.log('Second Word : ' + secondWord);
console.log('Third Word : ' + thirdWord);
console.log('Fourth Word : ' + fourthWord);
console.log('Fifth Word : ' + fifthWord)
console.log('Sixth Word : ' + sixthWord);
console.log('Seventh Word : ' + seventhWord);

//soal nomor 3 (mengurai kalimat menggunakan substring)
var sentence2 = 'wow Javascript is so cool';
var firstWord2 = sentence2.substring(0, 3);
var secondWord2 = sentence2.substring(4, 14);
var thirdWord2 = sentence2.substring(15, 18);
var fourthWord2 = sentence2.substring(18, 21);
var fifthWord2 = sentence2.substring(21, 26);

console.log('First Word : ' + firstWord2);
console.log('Second Word : ' + secondWord2);
console.log('Third Word : ' + thirdWord2);
console.log('Fourth Word : ' + fourthWord2);
console.log('Fifth Word : ' + fifthWord2);

//soal nomor 4 mengurai kalimat dan menentukan panjang string
var firstWordLength = firstWord2.length;
var secondWordLength = secondWord2.length;
var thirdWordLength = thirdWord2.length;
var fourthWordLength = fourthWord2.length;
var fifthWordLength = fifthWord2.length;

console.log('First Word : ' + firstWord2 + ', with length : ' + firstWordLength);
console.log('Second Word : ' + secondWord2 + ', with length : ' + secondWordLength);
console.log('Third Word : ' + thirdWord2 + ', with length : ' + thirdWordLength);
console.log('Fourth Word : ' + fourthWord2 + ', with length : ' + fourthWordLength);
console.log('Fifth Word : ' + fifthWord2 + ', with length : ' + fifthWordLength);

