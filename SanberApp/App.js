import React from 'react';
import { StyleSheet } from 'react-native';
// import Component from './Tugas/Tugas12/App'
import TugasTigaBelas from './Tugas/Tugas13/App'

export default function App() {
  return (
      // <Component />
      <TugasTigaBelas />
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
