import React from 'react'
import { View, StyleSheet, Text, Image } from 'react-native'

export default class About extends React.Component {
    render()
    {
        return(
            <View style={ styles.container }>
                <View style={ styles.appHeader }>
                    <View style={ styles.profileBox }>
                        <Image source={ require('../images/Profile.jpeg') } style={ styles.profilePic } />
                        <Text style={ { fontWeight: 'bold', marginTop: 30, fontSize: 18, } }>Taufik Rahadi</Text>
                        <Text style={ { fontSize: 14, color: '#bdc3c7' } }>Web Developer</Text>
                        <View style={ styles.socialMediaIcon }>
                            <Image source={ require('../images/instagram.png') } style={ styles.socialMediaImage } />
                            <Image source={ require('../images/twitter.webp') } style={ styles.socialMediaImage } />
                            <Image source={ require('../images/facebook.png') } style={ styles.socialMediaImage } />
                        </View>
                        <Text style={ { color: '#34ace0', marginTop: 20, fontWeight: '100', fontSize: 18 } }>View Skills</Text>
                    </View>
                </View>
                <View style={ styles.latestProject }>
                    <View style={ styles.latestProjectHeader }>
                        <Text style={ { fontWeight: 'bold', fontSize: 18 } }>Latest Project</Text>
                        <Text style={ { color: '#34ace0', marginTop: 20, fontWeight: '100', fontSize: 18, marginTop: -25, textAlign: 'right' } }>View All</Text>
                    </View>
                    <View style={ styles.projectBox }>
                        <Image source={ require('../images/project.png') } style={ styles.projectImg }/>
                        <Text style={ { fontWeight: 'bold', paddingLeft: 30, paddingTop: 10, fontSize: 18, } }>Project Name</Text>
                        <Text style={ { paddingLeft: 30, paddingTop: 5, fontSize: 14, color: '#bdc3c7' } }>Project Description</Text>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
    },
    appHeader: {
        backgroundColor: '#34ACE0',
        height: 234,
        borderBottomLeftRadius: 50,
        borderBottomRightRadius: 50,
        flexDirection: 'row',
        justifyContent: 'center'
    },
    profileBox: {
        backgroundColor: '#fff',
        height: 312,
        width: 272,
        marginTop: 100,
        elevation: 5,
        borderRadius: 30,
        alignItems: 'center'
    },
    profilePic: {
        marginTop: 40,
        height: 80,
        width: 80,
        borderRadius: 40,
    },
    socialMediaIcon: {
        marginTop: 20,
        flexDirection: 'row',
        paddingHorizontal: 30,
    },
    socialMediaImage: {
        height: 35,
        width: 35,
        marginHorizontal: 5,
    },
    latestProject: {
        marginTop: 210,
        paddingHorizontal: 50,
    },
    projectBox: {
        height: 219,
        backgroundColor: '#fff',
        borderRadius: 30,
        elevation: 3,
        marginTop: 35,
    },
    projectImg: {
        height: 144,
        width: 260,
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
    }
})