import React from 'react'
import { View, StyleSheet, Text, TextInput, Button, } from 'react-native'
import { color } from 'react-native-reanimated'

export default class Login extends React.Component {
    render()
    {
        return(
            <View style={ styles.container }>
                <View style={ styles.formHeader }>
                    <Text style={ styles.formHeaderText }>Hello,</Text>
                    <Text style={ styles.formHeaderText }>Welcome Back.</Text>
                </View>
                <View style={ styles.formBox }>
                    <View style={ styles.formSection }>
                        <Text style={ styles.formSectionLabel }>
                            Username
                        </Text>
                        <TextInput placeholder="Username" style={ styles.formSectionInput } />
                    </View>
                    <View style={ styles.formSection }>
                        <Text style={ styles.formSectionLabel }>
                            Password
                        </Text>
                        <TextInput placeholder="Password" secureTextEntry style={ styles.formSectionInput } />
                    </View>
                </View>
                <View style={ styles.loginButton }>
                    <Button
                        title="Login"
                        style={ { borderRadius: 7 } }
                    />
                    <Text style={ { textAlign: 'center', marginTop: 10, color: "#bdc3c7" } }>
                        Create Account
                    </Text>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 30,
    },
    loginButton: {
        marginTop: 30,
        elevation: 3,
    },
    formHeader: {
        justifyContent: 'center',
        height: 250,
    },
    formHeaderText: {
        fontFamily: 'sans-serif',
        fontWeight: 'bold',
        fontSize: 38,
    },
    formBox: {
        flexDirection: 'column',
        justifyContent: 'space-between',
    },
    formSection: {
        marginTop: 10,
        paddingTop: 20,
    },
    formSectionLabel: {
        fontWeight: '100',
        letterSpacing: 3,
        textTransform: 'uppercase',
        color: '#bdc3c7',
    },
    formSectionInput: {
        fontWeight: 'bold',
        marginTop: 10,
        width: '100%',
        paddingHorizontal: 10,
        borderStyle: 'solid',
        borderTopWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
        borderWidth: 1,
        borderColor: '#bdc3c7',
        height: 40,
    }
})