import React from 'react'
import { StatusBar, Text, View } from 'react-native'
import Login from './components/Login'
import About from './components/About'

export default class TugasTigaBelas extends React.Component {
    render() 
    {
        return(
            <View>
                <StatusBar />
                <Login />
                {/* uncomment baris dibawah */}
                {/* <About /> */}
            </View>
        )
    }
}