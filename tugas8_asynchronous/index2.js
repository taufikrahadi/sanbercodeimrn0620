var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 
let waktu = 10000
let i = 0

return readBooksPromise(waktu, books[0]).then(waktuSisa => {
    return readBooksPromise(waktuSisa, books[1]).then(waktuSisa => {
        return readBooksPromise(waktuSisa, books[2]).then(() => {
            console.log('promise nyiksa')
        })
    })
})