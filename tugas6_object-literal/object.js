console.log('soal 1 array to object')

function arrayToObject(data) {
    let i = 1
    let now = new Date()
    let processAge = (age) => {
        if(age == undefined) {
            return 'invalid birth year'
        }

        return (now.getFullYear() - age)
    }

    data.forEach(person => {
        console.log(`${i++}. ${person[0]} ${person[1]} : {`)
        console.log(`   firstName: ${person[0]}`)
        console.log(`   lastName: ${person[1]}`)
        console.log(`   gender: ${person[2]}`)
        console.log(`   age: ${processAge(person[3])}`)
        console.log('}')
        console.log()
    })
}

var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people)

var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
arrayToObject([])

console.log()
console.log('soal 2 shoopping time')

function shoppingTime(memberId, money) {
    if(memberId == undefined) {
        return "Mohon maaf, toko X hanya berlaku untuk member saja"
    } else if(money < 50000) {
        return "Mohon maaf, uang tidak cukup"
    }

    let products = [
        {
            name: 'Sepatu brand Stacattu',
            price: 1500000
        },
        {
            name: 'Baju brand Zoro',
            price: 500000
        },
        {
            name: 'Baju brand H&N',
            price: 250000
        },
        {
            name: 'Sweater brand Uniklooh',
            price: 175000
        },
        {
            name: 'Casing Handphone ',
            price: 50000
        }
    ]

    let listPurchased = []
    let total = 0

    products.forEach(product => {
        if(money > product.price) {
            listPurchased.push(product.name)
            total += product.price
        }
    })

    let output = {
        memberId: memberId,
        money: money,
        listPurchased: listPurchased,
        changeMoney: money - total
    }

    return output
}

console.log(shoppingTime('09asdlqwo', 2475000))
console.log(shoppingTime('82Ku8Ma742', 170000));

console.log()
console.log('soal 3 naik angkot')
function naikAngkot(listPenumpang) {
    if(listPenumpang == undefined) {
        return
    }

    let rute = ['A', 'B', 'C', 'D', 'E', 'F']

    let output = []

    listPenumpang.forEach(penumpang => {
        let obj = {
            penumpang: penumpang[0],
            naikDari: penumpang[1],
            tujuan: penumpang[2],
            bayar: 0
        }
        output.push(obj)
    })

    let bayar = (angka) => {
        return (angka + 1) * 2000
    }

    output.forEach(out => {
        out.bayar += bayar((rute.indexOf(out.tujuan) - (rute.indexOf(out.naikDari) + 1)))
    })

    return output
}

console.log(naikAngkot([['Icha', 'A', 'B'], ['Dimitri', 'B', 'F']]))
console.log()