/*========================================== 
  1. SOAL CLASS SCORE (10 poin + 5 Poin ES6)
  ==========================================
  Buatlah sebuah class dengan nama Score. class Score tersebut memiliki properti "subject", "points", dan "email". 
  "points" dapat di input berupa number (1 nilai) atau array of number (banyak nilai).
  tambahkan method average untuk menghitung rata-rata dari parameter points ketika yang di input berupa array (lebih dari 1 nilai)
*/

class Score {
    // Code disini
    constructor(points, subject, email) {
        this.points = points
        this.subject = subject
        this.email = email
    }

    average() {
        this.finalPoints = 0
        if(Array.isArray(this.points)) {
            this.points.forEach(point => {
                this.finalPoints += point
            })
            return this.finalPoints / this.points.length
        }
        return this.points
    }

    output() {
        return this.print = {
            email: this.email,
            subject: this.subject,
            points: this.points,
            average: this.average(),
        }
    }
}

let nilai = [80, 85, 80, 85]
let myScore = new Score(nilai, 'quiz-1', 'taufikrahadi@mail.com')
console.log(myScore.output())
  
  /*=========================================== 
    2. SOAL Create Score (10 Poin + 5 Poin ES6)
    ===========================================
    Membuat function viewScores yang menerima parameter data berupa array multidimensi dan subject berupa string
    Function viewScores mengolah data email dan nilai skor pada parameter array 
    lalu mengembalikan data array yang berisi object yang dibuat dari class Score.
    Contoh: 
  
    Input
     
    data : 
    [
      ["email", "quiz-1", "quiz-2", "quiz-3"],
      ["abduh@mail.com", 78, 89, 90],
      ["khairun@mail.com", 95, 85, 88]
    ]
    subject: "quiz-1"
  
    Output 
    [
      {email: "abduh@mail.com", subject: "quiz-1", points: 78},
      {email: "khairun@mail.com", subject: "quiz-1", points: 95},
    ]
  */
  
const data = [
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88],
    ["bondra@mail.com", 70, 75, 78],
    ["regi@mail.com", 91, 89, 93]
]

function viewScores(data, subject) {
    let output = []
    if(Array.isArray(data)) {
        data.forEach(person => {
            let score = new Score(person.slice(1, 4), subject, person[0])
            output.push(score.output())
            console.log()
        })
    }
    
    console.log(output)
}
  
  // TEST CASE
  viewScores(data, "quiz-1")
  viewScores(data, "quiz-2")
  viewScores(data, "quiz-3")
  
  /*==========================================
    3. SOAL Recap Score (15 Poin + 5 Poin ES6)
    ==========================================
      Buatlah fungsi recapScore yang menampilkan perolehan nilai semua student. 
      Data yang ditampilkan adalah email user, nilai rata-rata, dan predikat kelulusan. 
      predikat kelulusan ditentukan dari aturan berikut:
      nilai > 70 "participant"
      nilai > 80 "graduate"
      nilai > 90 "honour"
  
      output:
      1. Email: abduh@mail.com
      Rata-rata: 85.7
      Predikat: graduate
  
      2. Email: khairun@mail.com
      Rata-rata: 89.3
      Predikat: graduate
  
      3. Email: bondra@mail.com
      Rata-rata: 74.3
      Predikat: participant
  
      4. Email: regi@mail.com
      Rata-rata: 91
      Predikat: honour
  
  */
console.log()
function recapScores(data) {
    let i = 1
    let prediket = (nilai) => {
        let str = ''
    if(nilai > 90) {
        str = 'honour'
    } else if(nilai > 80) {
        str = 'graduate'
    } else if(nilai > 70) {
        str = 'participant'
    }

        return str
    }
    data.forEach(person => {
        let rata = new Score(person.slice(1, 4)).average()
        console.log(`${i++}. Email :    ${person[0]}`)
        console.log(`Rata - Rata :    ${rata}`)
        console.log(`Prediket :     ${prediket(rata)}`)
        console.log()
    })
}

recapScores(data);