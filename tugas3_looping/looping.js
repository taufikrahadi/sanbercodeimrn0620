// soal nomor 1 looping while
var str1 = "i love coding"
var str2 = "i will become a mobile developer"
var i = 0
var j = 22

//looping pertama
console.log('LOOPING PERTAMA')
while(i < 20) {
    i += 2
    console.log(`${i} - ${str1}`)
}

//looping kedua
console.log('LOOPING KEDUA')
while(j > 0) {
    j -= 2
    console.log(`${j} - ${str2}`)
}

//soal nomor 2 looping for
console.log('\n')
var str3 = ''

for(var k = 1; k <= 20; k++) {
    if((k % 2) == 0) {
        str3 = 'berkualitas'
    } else if((k % 3) == 0 && (k % 2) == 1) {
        str3 = 'i love coding'
    } else {
        str3 = 'santai'
    }

    console.log(`${k} - ${str3}`)
}

// soal nomor 3 membuat persegi panjang
console.log('\n')
var pagar = '#'

for(var m = 0; m < 4; m++) {
    for(var n = 0; n < 8; n++) {
        process.stdout.write(pagar)
    }
    console.log()
}

//soal nomor 4 membuat tangga
console.log('\n')

for(var o = 0; o < 7; o++) {
    for(var p = 0; p < o; p++) {
        process.stdout.write(pagar)
    }
    console.log()
}

// soal nomor 5 membuat papan catur
console.log('\n')
var s = ''

for(var r = 0; r < 8; r++) {
    if((r % 2) == 0) {
        s += ' '
    }
    for(q = 0; q < 4; q++) {
        s += '# '
    }
    s += '\n'
    
}

console.log(s)
