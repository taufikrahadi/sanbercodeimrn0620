// A. Balik String
function balikString(str) {
    let newStr = ''
    for( let i = str.length - 1; i >= 0; i-- ) {
        newStr += str[i]
    } 

    return newStr
}

console.log()
console.log(balikString("abcde")) // edcba
console.log(balikString("rusak")) // kasur
console.log(balikString("racecar")) // racecar
console.log(balikString("haji")) // ijah

// B. Palindrome
function palindrome(str) {
    if(str === balikString(str)) {
        return true
    }
    return false
}

console.log()
console.log(palindrome("kasur rusak")) // true
console.log(palindrome("haji ijah")) // true
console.log(palindrome("nabasan")) // false
console.log(palindrome("nababan")) // true
console.log(palindrome("jakarta")) // false

// C. Bandingkan Angka
function bandingkan(num, pos) {
    if((num == pos)) {
        return -1
    } else if(num < 0) {
        return -1
    } else if(pos < 0) {
        return -1
    } else if((num == undefined) || (pos == undefined)) {
        return num || pos
    } else {
        return num > pos ? num : pos
    }
}

console.log()
console.log(bandingkan(10, 15)); // 15
console.log(bandingkan(12, 12)); // -1
console.log(bandingkan(-1, 10)); // -1 
console.log(bandingkan(112, 121));// 121
console.log(bandingkan(1)); // 1
console.log(bandingkan()); // -1
console.log(bandingkan("15", "18")) // 18
