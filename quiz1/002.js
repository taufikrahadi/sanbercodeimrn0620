// soal 1
function ascendingTen(num) {
    let end = num + 10
    let output = ''

    if(num == undefined) {
        return -1
    }

    for( let i = num; i <= end; i++ ) {
        output += `${i} `
    }

    return output
}

console.log()
console.log(ascendingTen(11))
console.log(ascendingTen(21))
console.log(ascendingTen())

// soal 2
function descendingTen(num) {
    let end = num - 10
    let output = ''

    if(num == undefined) {
        return -1
    }

    for( let i = num; i >= end; i-- ) {
        output += `${i} `
    }

    return output
}

console.log()
console.log(descendingTen(100))
console.log(descendingTen(10))
console.log(descendingTen())

// soal 3
function conditionalAscendingDescending(num, check) {
    if(check == undefined || num == undefined) {
        return -1
    }

    let output = ''

    if((num % 2) == 0) {
        for( let i = num; i >= 0; i-- ) {
            output += `${i} `
        }
    } else {
        for( let i = num; i <= (num + 10); i++ ) {
            output += `${i} `
        }
    }

    return output
}

console.log()
console.log(conditionalAscendingDescending(10, 1))

// soal 4

