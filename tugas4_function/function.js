// soal nomor 1
function teriak() {
    return 'Halo Sanbers!'
}

console.log(teriak())

// soal nomor2
function multiply(angka1, angka2) {
    return angka1 * angka2
}

var num1 = 12
var num2 = 4
var hasilKali = multiply(num1, num2)
console.log(hasilKali)

// soal nomor 3
function introduce(name, age, address, hobby) {
    return `Nama saya ${name}, umur saya ${age} tahun, alamat saya di ${address}, saya punya hobby yaitu ${hobby}`
}

var name = 'Taufik'
var age = '20'
var address = 'Jl. Pesantren Perumahan Griya Utama Hangtuah 2 Pekanbaru'
var hobby = 'Rebahan'

var perkenalan = introduce(name, age, address, hobby)

console.log(perkenalan)
