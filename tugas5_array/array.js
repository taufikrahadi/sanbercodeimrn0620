compare = (startNum, finishNum) => {
    let start, finish, state
    if(startNum > finishNum) {
        start = finishNum
        finish = startNum
        state = false
    } else {
        start = startNum
        finish = finishNum
        state = true
    }

    return { start, finish, state }
}

//soal 1
console.log('\nSoal 1')

function range (startNum, finishNum) {
    if(startNum == null || finishNum == null) {
        return -1
    }

    let arr = []
    let compare = this.compare(startNum, finishNum)

    for(let i = compare.start; i <= compare.finish; i++) {
        arr.push(i)
    }
    return compare.state ? arr : arr.reverse()
}

console.log(range(20, 12))

//soal 2
console.log('\nSoal 2')

function rangeWithStep (startNum, finishNum, step) {
    let newArr = []
    let compare = this.compare(startNum, finishNum)

    for(let i = compare.start; i <= compare.finish;) {
        newArr.push(i)
        i += step
    }

    return newArr
}

console.log(rangeWithStep(1, 10, 3))

//soal 3
console.log('\nSoal 3')

function sum(awal, akhir, step) {
    let deret = []
    let jumlah = 0
    if(step == null) {
        step = 1
    }

    for(let i = awal; i <= akhir;) {
        deret.push(i)
        i += step
    }

    deret.forEach(der => {
        jumlah += der
    })

    return jumlah
}

console.log(sum(5, 50, 3))

//soal 4
console.log('\n Soal 4')

function dataHandling(data) {
    data.forEach(userData => {
        console.log(`Nomor ID : ${userData[0]}`)
        console.log(`Nama Lengkap : ${userData[1]}`)
        console.log(`TTL : ${userData[2]} ${userData[3]}`)
        console.log(`Hobi : ${userData[4]}`)
        console.log()
    })
}

let input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]

dataHandling(input)

//soal 5
console.log('Soal 5')

function balikKata(str) {
    let newStr = ''
    for(let i = str.length - 1; i >= 0; i--) {
        newStr += str[i]
    } 

    return newStr
}

console.log(balikKata("Haji ijah"))

//soal 6
console.log('\nSoal 6')
function dataHandling2(data) {
    let bulan = [
        'Januari', 
        'Februari', 
        'Maret', 
        'April', 
        'Mei', 
        'Juni', 
        'Juli', 
        'Agustus', 
        'September', 
        'Oktober',
        'November',
        'Desember'
    ]
    let elementBulan = data[3].split("")
    let getBulan = parseInt(elementBulan.slice(3, 5).join(""))
    let bulanToString = bulan[getBulan - 1]

    data[1] += ' Elsharawy'
    data[2] = `Provinsi ${data[2]}`
    data.pop()
    data.push('Pria')
    data.push('SMA Internasional Metro')
    elementBulan[2] = "-"
    elementBulan[5] = "-"
    let nama = data[1].split("")
    let newNama = []
    if(nama.length > 14) {
        for(let i = 0; i <= 14; i++) {
            newNama.push(nama[i])
        }
    }

    console.log(data)
    console.log(bulanToString)
    console.log(elementBulan.join(""))
    console.log(newNama.join(""))
}

dataHandling2(input[1])
