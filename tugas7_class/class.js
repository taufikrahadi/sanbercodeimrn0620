//release 1
class Animal 
{
    constructor(nama)
    {
        this.nama = nama
        this.legs = 4
        this.coldBlooded = false
    }

    get name()
    {
        return this.nama
    }
}

let sheep = new Animal("Shaun")
console.log(sheep.name)
console.log(sheep.legs)
console.log(sheep.coldBlooded)


//release 1
class Ape extends Animal 
{
    constructor(nama) 
    {
        super(nama)
        this.teriak = 'Auuuoo'
    }

    yell() 
    {
        console.log(this.teriak)
    }
}

class Frog extends Animal
{
    constructor(nama)
    {
        super(nama)
        this.lompat = 'Hop Hop'
    }

    jump ()
    {
        console.log(this.lompat)
    }
}

let sungokong = new Ape('Kera Sakti')
sungokong.yell()

let kodok = new Frog('Buduk')
kodok.jump()

// soal 2
class Clock
{
    constructor({ template })
    {
        this.template = template
    }

    render()
    {
        this.date = new Date()

        this.hours = this.date.getHours()
        this.hours < 10 ? this.hours = `0${this.mins}` : this.hours

        this.mins = this.date.getMinutes()
        this.mins < 10 ? this.mins = `0${this.mins}` : this.mins

        this.secs = this.date.getSeconds()
        this.secs < 10 ? this.mins = `0${this.secs}` : this.secs

        this.output = this.template
                        .replace('h', this.hours)
                        .replace('m', this.mins)
                        .replace('s', this.secs)
        
        console.log(this.output)
    }

    start()
    {
        this.render()
        this.timer = setInterval(() => this.render(), 1000)
    }

    stop()
    {
        clearInterval(this.timer)
    }
}

let clock = new Clock({ template: 'h:m:s' })
clock.start()
